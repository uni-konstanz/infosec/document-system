#!/bin/bash
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained
# with the works, so that any entity that uses the works is notified of this
# instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# documentation.sh
# Collects system information and creates files for system documentation.
###

# create or empty files
DIR="."
OS="${DIR}/operatingsystem.txt"
PKGS="${DIR}/packages.txt"
NET="${DIR}/network.txt"

echo -n "" > "${OS}"
echo -n "" > "${PKGS}"
echo -n "" > "${NET}"

### HOST INFORMATION

# fully qualified hostname
printf "Hostname\n" | tee -a "${OS}"
hostname -f  | tee -a "${OS}"

### OPERATING SYSTEM

# system information
printf "\nBetriebssystem\n"  | tee -a "${OS}"
uname -a  | tee -a "${OS}"
lsb_release -a  | tee -a "${OS}"


### USERS AND GROUPS
printf "\nNutzer und Gruppen\n"  | tee -a "${OS}"

# show all service users and their groups
awk -F: '($3>=1000)&&($1!="nobody"){print $1}' /etc/passwd | xargs groups | tee -a "${OS}"

# show all users with sudo rights
#getent group sudo | cut -d: -f4 >> "${OS}"
#getent group sudo  | tee -a "${OS}"

# show all remote ssh users (only works for
#getent group ssh | cut -d: -f4 >> "${OS}"
#getent group ssh  | tee -a "${OS}"


### PACKAGES

# list installed packages
printf "Installierte Packete\n"  | tee -a "${PKGS}"
apt list --installed  | tee -a "${PKGS}"

# list installed pip packages
printf "\nInstallierte PIPs\n"  | tee -a "${PKGS}"
pip list --format=columns   | tee -a "${PKGS}"

# list installed gems
printf "\nInstallierte Gems\n"  | tee -a "${PKGS}"
gem list --local  | tee -a "${PKGS}"


### NETWORK CONFIGURATION

# show interfaces, ips, routes, nameserver, timeserver
printf "Netzwerkschnittstellen\n"  | tee -a "${NET}"
#ip -o addr | awk '/inet /{print "IP (" $2 "):\t" $4}'
ip -o addr | awk '{print $2 " (" $3 "):\t" $4}'  | tee -a "${NET}"

printf "\nRouting\n" | tee -a "${NET}"
ip route  | tee -a "${NET}"

printf "\nDNS\n" | tee -a "${NET}"
tee -a "${NET}" < /etc/resolv.conf
#cat /etc/resolv.conf  | tee -a "${NET}"

printf "\nTimeserver\n" | tee -a "${NET}"
grep NTP /etc/systemd/timesyncd.conf | tee -a "${NET}"
#cat /etc/systemd/timesyncd.conf | grep NTP  | tee -a "${NET}"

# list listening services
printf "\nAktive TCP Verbindungen\n"   | tee -a "${NET}"
netstat -ntlp | tail -n +3 | cat -n  | tee -a "${NET}"

printf "\nAktive UDP Verbindungen\n"   | tee -a "${NET}"
netstat -nulp | tail -n +3 | cat -n  | tee -a "${NET}"

printf "\nAktive Socket Verbindungen\n"   | tee -a "${NET}"
netstat -nxlp | tail -n +3 | cat -n  | tee -a "${NET}"

# list running services
printf "\nLaufende Dienste\n"   | tee -a "${NET}"
systemctl list-units --type=service --state=running --no-pager  | tee -a "${NET}"

# show firewall
printf "\nFirewall Konfiguration\n"  | tee -a "${NET}"
sudo ufw status  | tee -a "${NET}"
